# Conductor Local Setup (For Windows)

**Step 1**: Clone project: https://truongtn@bitbucket.org/truongtn/conductor-setup.git and extract file ***ConductorSetup***.

**Step 2**: In ***gradle*** folder go to ***bin***. Copy folder link (ex: C:\Users\TruongTN\Desktop\ConductorSetup\gradle\bin)

**Step 3**: Open ***Edit the system environment variables***. Choose ***Environment Variables***. 
In ***System Variables*** choose ***path*** and then add below link to system path.

**Step 4**: If you have not yet install Node JS, go to ***Node JS*** folder and run the following file in order: ***node-v8.9.1-x64***,***firsttime1.bat***, ***firsttime2.bat***, ***firsttime3.bat***. or else you only have to run: ***firsttime1.bat***, ***firsttime2.bat***, ***firsttime3.bat***.

**Step 5**: In ***Conductor Setup*** folder, run ***step1.bat***. After finished step1.bat, run **step2.bat**. (step1.bat only need to run till 70%)

*Note*: From the second time, you only need to run from ***step 5***
